cd UI
call Make_Python_Ui.bat
cd ..
del BtEmipGenerator.exe
rmdir /Q /S dist
py -3 -m PyInstaller btempgen.spec
rmdir /Q /S build
copy /B /Y dist\BtEmipGenerator.exe .
rmdir /Q /S dist