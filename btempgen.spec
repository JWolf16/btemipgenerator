# -*- mode: python -*-

block_cipher = None


a = Analysis(['btemipgenerator.py'],
             pathex=['C:\\Users\\sisko\\PycharmProjects\\btemipgenerator'],
             binaries=[],
             datas=[('unitypack/strings.dat', 'unitypack'),
             ('unitypack/structs.dat', 'unitypack'),
             ('unitypack/classes.json', 'unitypack')
             ],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='BtEmipGenerator',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          runtime_tmpdir=None,
          console=False , icon='icon.ico')
