# BT EMIP Generator

a Utility for generating a mod package that can be injected into a Unity Asset Bundle by UABE.

**Note: This tool is specifically aimed at HBS's BattleTech PC game for helping with the import of new mechs into to game**

## How to use

**Note: if you have already done these steps and have left the tool open and have made edits to the selected bundles and want to re-examine them, skip to step 5**

    1. For the Parts Bundle, select the asset bundle containing your new mech
    
    2. For the Donor Bundle, select the bundle you want to inject the mech parts into, this should be a bundle that UABE has already uncompressed (you may have already done a manifest edit on it)
    **Note: in some cases the tool may fail to load this bundle, if that occurs try re-saving the bundle in UABE**
    
    3. For the Blank Mesh select the mesh dat file that will be used to blank unused assets in the Donor
    
    4. set the desired state of the Options
    
    5. press 'Load Assets'
    
    6. Wait for the bundles to load and a remapping to be done (on my system this takes 5 - 20 seconds)
    
    7. Manually review the assets to be re-mapped as needed (right click an item to remove it from the generation)
    
    8. press 'Generate EMIP' and select a file to save the output to.
    
    9. Open UABE (all further steps are done in UABE), if UABE was already open close any open bundles you have open
    
    10. select 'Load Package File', and select the emip file generated in step 8
    
    11. Check the 'Affected bundles' checkbox, then press Ok
    
    12. Quickly verify the emip was correctly applied by hitting info and sortting on the 'modified column', a number of meshes should have * in the column
    
    13. press ok, then save the bundle in UABE.
    
    14. close the bundle then open your saved bundle if you want to check it was created correctly.
    
    
    
## About Body Mapping

Mech Body Meshes are identified as meshes that do not end in _eh, _bh, _mh.

parts are then remapped to the donor based on their names (after their first underscore). parts that match in both the donor and the
parts bundles are mapped to each other as are parts that use the _dmg suffix (but otherwise match), _explodes are also
optionally matched this way. center torso pieces are the exception where center or centre spellings will still be matched.
all body meshes in the donor that are not matched to a part in the parts bundle will be blanked.

example:
```
fs9_right_leg_thigh -> frb_right_leg_thigh
fs9_right_leg_thigh_dmg -> frb_right_leg_thigh
fs9_right_torso -> Blanked
```


## About Weapon Mapping

Mech weapons are similarly matched, except the matching is done using the 'container' of asset in the donor bundle (thus this
requires that the manifest has already been edited)

Note: container names are chopped at the second underscore character to remove the 'chrprfweap' prefix and mech name.

This is then matched to an asset name within the parts bundle (with the mech model prefix chopped of)

example:
```
fs9_right_torso_uac5_bh1 has the container chrprfweap_firebee_righttorso_ppc_eh2.prefab, this is matched to frb_righttorso_ppc_eh2 in the parts bundle

```

## Options

```
Blank Explodes: Replace _explodes parts with blanks instead of the base part

Use Base for Dmg Parts: When checked base parts will be used for _dmg parts, if unchecked the parts bundle should include
_dmg parts or else they will be blanked

Alternative Weapon Matches: Best used for importing weapons on a donor that has already had weapons imported before
```