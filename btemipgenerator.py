from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

import sys
import time
import os
import os.path
import logging
import logging.handlers

import traceback

from emipgen import Ui_EmipGeneUi
from libemipmaker import BkProcessor, RemapTableModel, RemapProxyModel

STR_VERSION = '1.1.4-20200807'

rootLogger = logging.getLogger('BTEG')
rootLogger.setLevel(logging.INFO)
obj_handler = logging.FileHandler('BTEGLog.txt', mode='w')
obj_formatter = logging.Formatter('%(levelname)-10s (%(module)-20s:%(funcName)-30s:%(lineno)-4d) %(message)s')
obj_handler.setFormatter(obj_formatter)
obj_handler.setLevel(logging.DEBUG)
rootLogger.addHandler(obj_handler)


class BtEmipGenerator(QMainWindow, Ui_EmipGeneUi):
    """
    Main UI

    """

    def __init__(self, obj_qapp, logger, parent=None):
        """

        :param obj_qapp:
        :type obj_qapp:
        :param logger:
        :type logger: logging.Logger
        :param parent:
        :type parent:
        """
        super(BtEmipGenerator, self).__init__(parent)
        self.setupUi(self)
        self.qapp = obj_qapp
        self.setWindowTitle('BT EMIP Generator v' + STR_VERSION)
        self.ProgressDialog = None
        self.Logger = logger
        self.lastDir = ''

        self.setSkin('Fusion')

        self.bkThread = QThread()
        self.bkProcessor = BkProcessor(self.Logger)
        self.bkProcessor.moveToThread(self.bkThread)
        self.bkThread.start()

        self.dataModel = RemapTableModel()
        self.dataModel.update_table(self.bkProcessor.emipGen)

        self.proxyModel = RemapProxyModel()
        self.proxyModel.setSourceModel(self.dataModel)

        # set up the inventory table
        self.tableView.setModel(self.proxyModel)
        self.tableView.verticalHeader().show()
        # self.tableView.horizontalHeader().setResizeMode(2, QHeaderView.Stretch)
        self.tableView.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.tableView.horizontalHeader()
        self.tableView.setSortingEnabled(True)

        self.tableView.setContextMenuPolicy(Qt.ActionsContextMenu)

        # add remove actions to the right-click menu
        actionRemove = QAction(self.tableView)
        actionRemove.setText('Remove Selected Item')
        actionRemove.triggered.connect(self._removeItem)
        self.tableView.addAction(actionRemove)

        self.pushButton_partsSelect.pressed.connect(self.selectParts)
        self.pushButton_donorSelect.pressed.connect(self.selectDonor)
        self.pushButton_blankSelect.pressed.connect(self.selectblank)
        self.pushButton_load.pressed.connect(self.startBundleLoad)
        self.pushButton_export.pressed.connect(self.writeEmip)

        self.bkProcessor.BkUpdate.connect(self.bkUpdate)
        self.bkProcessor.BundleLoadDone.connect(self.bundleLoadFinished)

        QShortcut(QKeySequence(Qt.CTRL + Qt.ALT + Qt.Key_D), self, self.setDebugFlag)

    def setDebugFlag(self):
        self.setWindowTitle('BT EMIP Generator - DEBUG v' + STR_VERSION)
        self.Logger.setLevel(logging.DEBUG)

    def setFusionPalette(self):
        palette = QPalette()
        palette.setColor(QPalette.Window, QColor(53, 53, 53))
        palette.setColor(QPalette.WindowText, Qt.white)
        palette.setColor(QPalette.Base, QColor(15, 15, 15))
        palette.setColor(QPalette.AlternateBase, QColor(53, 53, 53))
        palette.setColor(QPalette.ToolTipBase, Qt.white)
        palette.setColor(QPalette.ToolTipText, Qt.white)
        palette.setColor(QPalette.Text, Qt.white)
        palette.setColor(QPalette.Button, QColor(53, 53, 53))
        palette.setColor(QPalette.ButtonText, Qt.white)
        palette.setColor(QPalette.BrightText, Qt.red)
        palette.setColor(QPalette.PlaceholderText, Qt.gray)

        palette.setColor(QPalette.Highlight, QColor(142, 45, 197).lighter())
        palette.setColor(QPalette.HighlightedText, Qt.black)

        palette.setColor(QPalette.Disabled, QPalette.ButtonText, Qt.darkGray)
        palette.setColor(QPalette.Disabled, QPalette.PlaceholderText, Qt.darkGray)
        palette.setColor(QPalette.Disabled, QPalette.Text, Qt.darkGray)
        palette.setColor(QPalette.Disabled, QPalette.WindowText, Qt.darkGray)

        self.qapp.setPalette(palette)

    def updateSkin(self):
        """
        change the skin of the UI

        :return:
        :rtype:
        """
        str_skin = self.sender().text()
        self.setSkin(str_skin)

    def setSkin(self, str_skin):
        """
        apply a stylesheet operation

        :param str_skin: the skin to change to, eithr an option in self.skinStyles or a builtin one
        :type str_skin: str
        :return:
        :rtype:
        """
        # print 'Selecting Skin: {0}'.format(str_skin)
        if str_skin in QStyleFactory.keys():
            self.qapp.setStyle(QStyleFactory.create(str_skin))
            self.qapp.setStyleSheet("")
            if str_skin == 'Fusion':
                self.setFusionPalette()
            else:
                self.qapp.setPalette(self.style().standardPalette())
        else:
            self.qapp.setPalette(self.style().standardPalette())
            self.qapp.setStyle(QStyleFactory.create('windowsvista'))
            mfile = QFile(":/stylesheets/{0}".format(str_skin))
            mfile.open(QFile.ReadOnly)
            style = mfile.readAll()
            self.qapp.setStyleSheet(style.data().decode('utf8'))
            mfile.close()

    def selectParts(self):
        str_write_dir = QFileDialog.getOpenFileName(self, caption="Select Parts AssetBundle",
                                                    dir=self.lastDir)[0]

        if str_write_dir != '':
            self.lastDir = os.path.dirname(str_write_dir)
            self.lineEdit_parts.setText(str_write_dir)

    def selectDonor(self):
        str_write_dir = QFileDialog.getOpenFileName(self, caption="Select Donor AssetBundle",
                                                    dir=self.lastDir)[0]

        if str_write_dir != '':
            self.lastDir = os.path.dirname(str_write_dir)
            self.lineEdit_donorBundle.setText(str_write_dir)

    def selectblank(self):
        str_write_dir = QFileDialog.getOpenFileName(self, caption="Select Blank Mesh",
                                                    dir=self.lastDir)[0]

        if str_write_dir != '':
            self.lastDir = os.path.dirname(str_write_dir)
            self.lineEdit_blankData.setText(str_write_dir)

    def writeEmip(self):
        str_write_dir = QFileDialog.getSaveFileName(self, caption="Save EMIP File", filter="UABE Mod Package (*.emip)",
                                                    dir=self.lastDir)[0]

        if str_write_dir != '':
            self.bkProcessor.emipGen.setOutFile(str_write_dir)
            self.bkProcessor.emipGen.writeEmipFile(self.lineEdit_donorBundle.text())
            QMessageBox.about(self, 'Success', 'EMIP File written, Apply in UABE')

    def _removeItem(self):
        lst_keys = []
        for item in sorted(self.tableView.selectedIndexes()):
            obj_idx = self.proxyModel.mapToSource(item)
            # self.dataModel.printRecord(obj_idx)
            lst_keys.append(obj_idx.row())
        for key in lst_keys:
            self.dataModel.removeRecord(key)
        self.dataModel.refresh()

    def bkUpdate(self, str_message):
        """
        update the progress dialog text during a background operation

        :param str_message: the message to display
        :type str_message: str
        :return:
        :rtype:
        """
        if self.ProgressDialog is not None:
            self.ProgressDialog.setLabelText(str_message)

    def closeEvent(self, event):
        """
        handle shutdown gracefully

        :param event:
        :type event:
        :return:
        :rtype:
        """
        self.bkThread.terminate()
        while not self.bkThread.isFinished():
            time.sleep(0.05)
        event.accept()

    def bundleLoadFinished(self, iResult):
        if self.ProgressDialog is not None:
            self.ProgressDialog.reset()
        self.pushButton_export.setEnabled(False)
        self.dataModel.update_table(self.bkProcessor.emipGen)
        if iResult == BkProcessor.Success:
            self.pushButton_export.setEnabled(True)
        elif iResult == BkProcessor.Failed:
            QMessageBox.critical(self, 'Failed to Load Bundles', 'Bundles could not be loaded, please send logs to Jamie Wolf')
        elif iResult == BkProcessor.PartsFailed:
            QMessageBox.warning(self, 'Failed to load parts bundle', 'Parts bundle failed to load, please ensure, the selected bundle exists and is an assetbundle')
        elif iResult == BkProcessor.DonorFailed:
            QMessageBox.warning(self, 'Could not load donor bundle', 'Donor bundle could not be loaded\n try opening bundle in uabe and re-save it')
        else:
            QMessageBox.about(self, 'Something Happened!', 'No clue how you got this but it would awesome if you could report me!')

    def startBundleLoad(self):
        if self.lineEdit_blankData.text() == '' or self.lineEdit_donorBundle.text() == '' or self.lineEdit_parts.text() == '':
            return
        self.bkProcessor.PartsBundle = self.lineEdit_parts.text()
        self.bkProcessor.DonorBundle = self.lineEdit_donorBundle.text()
        self.bkProcessor.BlankFile = self.lineEdit_blankData.text()
        self.bkProcessor.BlankExplodes = self.checkBox_blankExplodes.isChecked()
        self.bkProcessor.ApplyBody = self.checkBox_body.isChecked()
        self.bkProcessor.Applyweapons = self.checkBox_weapons.isChecked()
        self.bkProcessor.AltWeaponPattern = self.checkBox_altWeaponMatches.isChecked()
        self.bkProcessor.BaseDmgParts = self.checkBox_dmgParts.isChecked()
        self.bkProcessor.hbsWhyMode = self.checkBox_hbsWhy.isChecked()
        self.bkProcessor.blankUnmatchedWeapons = self.checkBox_blankWeapons.isChecked()
        self.ProgressDialog = QProgressDialog('Starting Bundle Load', 'Cancel', 0, 0, self)
        self.ProgressDialog.setWindowFlags(self.ProgressDialog.windowFlags() & ~Qt.WindowContextHelpButtonHint)
        self.ProgressDialog.setLabelText('Loading Bundle...')
        self.ProgressDialog.setCancelButton(None)
        self.ProgressDialog.setWindowTitle('Loading Data')
        self.ProgressDialog.setWindowModality(Qt.WindowModal)
        self.ProgressDialog.showNormal()
        self.bkProcessor.StartBundleLoad.emit()

if __name__ == '__main__':
    rootLogger.info('----- BT EMIP Generator v{0} -----'.format(STR_VERSION))

    QApplication.setAttribute(Qt.AA_EnableHighDpiScaling, False)

    obj_main_app = QApplication(sys.argv)
    obj_main_window = BtEmipGenerator(obj_main_app, rootLogger)
    obj_main_window.show()
    obj_main_app.exec_()
