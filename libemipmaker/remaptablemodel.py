from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *
import time
from .emipgen import EmipGenerator


class RemapTableModel(QAbstractTableModel):
    """
    inventory table model

    """

    def __init__(self, parent=None):
        super(RemapTableModel, self).__init__(parent)
        self.Header = ['Donor Asset', 'New Asset']
        self.empgen = EmipGenerator()
        self.inventoryRecords = []
        self.keyIdex = []


    def update_table(self, obj_data):
        """
        change the inventory data loaded into the model

        :param obj_data:
        :type obj_data: EmipGenerator
        :return:
        :rtype:
        """
        fl_st = time.time()
        self.empgen = obj_data
        self.refresh()
        # print 'Table Update Op took {0:02f} seconds'.format(time.time() - fl_st)


    def refresh(self):
        """
        refresh the table

        :return:
        :rtype:
        """
        self.beginResetModel()
        self.endResetModel()

    def rowCount(self, *args, **kwargs):
        return len(self.empgen.editToWrite)

    def columnCount(self, *args, **kwargs):
        return len(self.Header)

    def flags(self, index):
        """
        set flags for rows and columns in the table

        :param index: the index of the cell
        :type index:
        :return:
        :rtype:
        """
        if not index.isValid():
            return None
        return super(RemapTableModel, self).flags(index)

    def data(self, index, role):
        if not index.isValid():
            return None
        elif role not in [Qt.DisplayRole, Qt.EditRole, Qt.ItemDataRole]:
            return None
        record = self.empgen.editToWrite[index.row()]
        if index.column() == 0:
            return str(record.origName)
        elif index.column() == 1:
            return str(record.name)
        else:
            return None

    def setData(self, index, value, role):
        return False

    def headerData(self, col, orientation, role):
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return self.Header[col]
        elif orientation == Qt.Vertical and role == Qt.DisplayRole:
            return col
        return None

    def removeRecord(self, int_key):
        self.empgen.editToWrite.pop(int_key)