import unitypack
import pprint
from io import BytesIO


class ObjectData(object):

    CompRemap = {
        'left_arm' : 'leftarm',
        'right_arm' : 'rightarm',
        'forearm_' : '',
        'left_torso' : 'lefttorso',
        'right_torso' : 'righttorso',
        'center_torso' : 'centertorso',
        'centre_torso' : 'centertorso'
    }

    HbsWhyYouDoThisItems =[
        '_up',
        '_right',
        '_left',
        '_down',
        '_ppc', '_mg', '_gauss', '_lbx',
        '_uac5', '_ac5', '_ac10', '_ac20'
    ]

    def __init__(self):
        self.name = ''
        self.data = b''
        self.id = 0
        self.type = 0
        self.origName = ''
        self.path = ''

    def pprint(self):
        return 'Name: {0}, Size: {1}, tId: {2}'.format(self.name, len(self.data), self.type)

    def __repr__(self):
        return self.pprint()

    @property
    def compName(self):
        strTmp = self.name.split('_', 1)[-1]
        for item in self.CompRemap:
            strTmp = strTmp.replace(item, self.CompRemap[item])
        return strTmp

    def altCompare(self, stName, stContainer, stAltItemId, bUseHbsWhy=False):
        stComp = self.compName
        if 'missile' in stComp:
            stLrm = stComp.replace('missile', 'lrm')
            stSrm = stComp.replace('missile', 'srm')
            if stLrm == stName or stSrm == stName:
                return True
        elif stName.endswith('_ah', 0, len(stName) - 1):
            tmp = list(stName)
            tmp[-3] = 'b'
            stBh = ''.join(tmp)
            tmp[-3] = 'e'
            stEh = ''.join(tmp)
            tmp[-3] = 'm'
            stMh = ''.join(tmp)
            if stComp in [stBh, stEh, stMh]:
                return True
        elif bUseHbsWhy:
            # print(f'{stContainer}, {stName}, {stAltItemId}, {stComp}, {self.name}')
            # for suffix in self.HbsWhyYouDoThisItems:
            #     if stComp.endswith(suffix):
            #         stPrefix = stComp[0:int(f'-{(len(suffix))}')]
            #         for x in range(9):
            #             toComp.append(stPrefix + f'_bh{x}')
            #             toComp.append(stPrefix + f'_eh{x}')
            #             toComp.append(stPrefix + f'_mh{x}')
            # if len(toComp) > 0:
            #     print(toComp)
            if self.name == stAltItemId:
                # print('Matched!')
                # print(f'hbs why matched: {stContainer}, {stName}, {stAltItemId}, {stComp}, {self.name}')
                return True
        return False


    @property
    def bytes(self):
        """

        :return:
        :rtype: bytes
        """
        bio = BytesIO()
        # This seems to be what UABE does
        bio.write(b'\x02\0\x01\x01\0\0\0\0')
        bio.write(self.id.to_bytes(8, byteorder='little', signed=True))
        bio.write(self.type.to_bytes(4, byteorder='little', signed=True))
        # no clue what this is but seems to be constant
        bio.write(b'\xff\xff\0\0\0\0\0\0\0\0')
        bio.write(len(self.data).to_bytes(4, byteorder='little'))
        # again no clue
        bio.write(b'\0\0\0\0')
        bio.write(self.data)
        return bio.getvalue()


class ManifestEntry(object):

    def __init__(self):
        self.name = ''
        self.assetId = None
        self.altMapId = ''

    def __repr__(self):
        # dat = self.getMap()
        # return 'Container: {0}, Mesh: {1}'.format(dat[0], dat[1])
        try:
            return f'Name: {self.name}, asset: {self.assetId}, assetName: {self.assetId.object.read().name}'
        except:
            return f'Name: {self.name}, asset: {self.assetId}, assetName: {self.assetId.object}'

    def getAssetName(self):
        try:
            return self.assetId.object.read().name
        except:
            return ''

    def populateAltMap(self):
        if self.getAssetName().lower().startswith('chrprfweap_'):
            obj = self.assetId.object.read()
            for item in obj.component:
                data = item['component'].object.read()
                try:
                    for render in data['renderers']:
                        self.altMapId = render.object.read().game_object.object.read().name
                except Exception as e:
                    pass


    def _chopModel(self, strIn):
        if strIn is None:
            return ''
        strTmp = strIn.lower().split('chrprfweap_')[-1]
        return strTmp.split('_', 1)[-1].replace('.prefab', '')

    def getMap(self):
        return [self._chopModel(self.name), self._chopModel(self.assetId.object.read().name), self.altMapId]





class AssetData(object):

    def __init__(self, type, bundlePath, mapType=None):
        self.filePath = bundlePath
        self.typeToIndex = type
        self.objects = {} # type: dict[int, ObjectData]
        self.id = 0
        self.name = ''
        self.bundleManifest = None # type: list[ManifestEntry]
        self.MapType = mapType
        if self.MapType is None:
            self.MapType = ''
        self.MapObjects = []

    def load(self):
        bundleManifest = None
        with open(self.filePath, "rb") as f:
            bundle = unitypack.load(f)
            for asset in bundle.assets:
                if self.name == '':
                    self.name = asset.name
                for id, object in asset.objects.items():
                    if object.type == self.typeToIndex:
                        data = object.read()
                        oData = ObjectData()
                        oData.name = data.name
                        asset._buf.seek(asset._buf_ofs + object.data_offset)
                        oData.data = asset._buf.read(object.size)
                        oData.id = id
                        oData.type = object.type_id
                        oData.path = object.path_id
                        self.objects[id] = oData
                    elif object.type == 'AssetBundle':
                        bundleManifest = object.read()
                    elif object.type == self.MapType:
                        item = object.read()
                        if item.mesh is not None:
                            self.MapObjects.append(object.read())
                            # print(f'Hit: {len(self.MapObjects)}, {self.MapObjects[-1].mesh}, {self.MapObjects[-1].game_object}')
        if bundleManifest is not None:
            self.bundleManifest = []
            for item in bundleManifest['m_Container']:
                tmp = ManifestEntry()
                tmp.name = item[0]
                tmp.assetId = item[1]['asset']
                tmp.populateAltMap()
                self.bundleManifest.append(tmp)

    def getObjectsFiltered(self, suffixFilters, bEquals):
        """

        :param suffixFilters:
        :type suffixFilters: tuple
        :param bEquals:
        :type bEquals: bool
        :return:
        :rtype: list[ObjectData]
        """
        ret = []
        for item in self.objects:
            # print(self.objects[item].name)
            if self.objects[item].name.split('_')[-1].startswith(suffixFilters):
                if bEquals:
                    # print(f"found: {self.objects[item].name}")
                    ret.append(self.objects[item])

            else:
                if not bEquals:
                    ret.append(self.objects[item])
                # else:
                #     print(f"disgard: {self.objects[item].name}")
        return ret

    def pprint(self, suffixFilters=None):
        ret = 'Bundle: {0}\n'.format(self.filePath)
        ret += 'Filter: {0}\n'.format(self.typeToIndex)
        ret += 'Found: {0}\n'.format(len(self.objects))
        ret += '---- Objects -----\n'
        for item in self.objects:
            if suffixFilters is not None:
                if self.objects[item].name.split('_')[-1].startswith(suffixFilters):
                    continue
            ret += '\t- Id: {0}, {1}\n'.format(item, self.objects[item].pprint())

        return ret