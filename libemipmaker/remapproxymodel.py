from PySide2.QtCore import Qt, QSortFilterProxyModel


class RemapProxyModel(QSortFilterProxyModel):

    def __init__(self, parent=None):
        QSortFilterProxyModel.__init__(self, parent)

    def lessThan(self, left, right):
        leftData = self.sourceModel().data(left, Qt.ItemDataRole)
        rightData = self.sourceModel().data(right, Qt.ItemDataRole)

        return leftData < rightData

    # def filterAcceptsRow(self, sourceRow, sourceParent):
    #     if not self.HideZero:
    #         return True
    #
    #     index = self.sourceModel().index(sourceRow, 3, sourceParent)
    #     value = int(self.sourceModel().data(index, Qt.ItemDataRole))
    #     return value != 0