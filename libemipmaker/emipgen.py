from .assetdata import AssetData, ObjectData
from io import BytesIO
import os
import os.path


class EmipGenerator(object):

    def __init__(self, outFile=None, assetName=None):
        """

        :param outFile:
        :type outFile: str
        """
        self.outFile = outFile
        if self.outFile is not None:
            if not self.outFile.endswith('.emip'):
                self.outFile += '.emip'
        self.editToWrite = []  # type: list[ObjectData]
        self.assetModified = assetName

    def setOutFile(self, outFile):
        self.outFile = outFile
        if not self.outFile.endswith('.emip'):
            self.outFile += '.emip'

    def addReplacements(self, dataMap, newObjects, DonorObjects):
        """
        :param dataMap:
        :type dataMap: dict[str, list[str]]
        :param newObjects:
        :type newObjects: list[ObjectData]
        :param DonorObjects:
        :type DonorObjects: list[ObjectData]
        :return:
        :rtype:
        """
        for item in dataMap:
            for replacement in newObjects:
                if replacement.name == item:
                    for donor in DonorObjects:
                        if donor.name in dataMap[item]:
                            oData = ObjectData()
                            oData.name = item
                            oData.id = donor.id
                            oData.data = replacement.data
                            oData.type = donor.type
                            oData.origName = donor.name
                            self.editToWrite.append(oData)
                    break

    def addBlanks(self, blankData, toBlank, DonorObjects):
        """

        :param blankData:
        :type blankData: bytes
        :param toBlank:
        :type toBlank: list[str]
        :param DonorObjects:
        :type DonorObjects: list[ObjectData]
        :return:
        :rtype:
        """
        for item in toBlank:
            for donor in DonorObjects:
                if donor.name == item:
                    oData = ObjectData()
                    oData.name = 'Blanked'
                    oData.id = donor.id
                    oData.data = blankData
                    oData.type = donor.type
                    oData.origName = donor.name

                    self.editToWrite.append(oData)
                    break

    def writeEmipFile(self, bundlePath):
        bio = BytesIO()
        # Magic number for UABE, nulls seem to be for optional info
        bio.write(b'EMIP\0\0\0\0')
        bio.write((1).to_bytes(4, byteorder='big')) # no clue
        bio.write(b'\0\0\0\x01')  # this is for an assetbundle, 0 is for assets
        path = os.path.abspath(bundlePath).encode('utf-8')
        bio.write(len(path).to_bytes(2, byteorder='little'))
        bio.write(path)
        bio.write(b'\x01\0\0\0\x04\0\0') # todo try to figure what the hell this is? the 1 looks to be little endian for the number of assets being changed, to 04 and other 2 bytes no clue
        nCablength = len(self.assetModified)
        bio.write(nCablength.to_bytes(2, byteorder='little'))
        bio.write(self.assetModified.encode('utf-8'))
        # seems to need to be written twice, probably a start and end asset index?
        bio.write(nCablength.to_bytes(2, byteorder='little'))
        bio.write(self.assetModified.encode('utf-8'))
        bio.write(b'\x01') # no clue, maybe signifies data start of some type?
        # uabe seems to use 8 bytes for this,
        bio.write(len(self.editToWrite).to_bytes(8, byteorder='little'))

        edits = sorted(self.editToWrite, key=lambda x: x.id)

        mfile = open(self.outFile, 'wb')
        mfile.write(bio.getvalue())
        for edit in edits:
            mfile.write(edit.bytes)
        mfile.close()
