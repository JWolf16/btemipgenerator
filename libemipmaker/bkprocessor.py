from PySide2.QtCore import QObject, Signal, Slot, QTimer, QMutex
from PySide2.QtWidgets import QApplication
from .emipgen import EmipGenerator
from .assetdata import AssetData, ObjectData
import traceback
import pprint

HASH_TABLE_DEBUG_OUT = False
INSERT_SAVE_CLEAN = False

class BkProcessor(QObject):

    StartBundleLoad = Signal()
    BundleLoadDone = Signal(int)

    BkUpdate = Signal(str)
    BkPercent = Signal(int)

    Success = 0
    Failed = 1
    DonorFailed = 2
    PartsFailed = 3

    bodyPartsFilter = ('eh', 'mh', 'bh', 'ah')
    bodyPartsFilterHbsWhy = ('eh', 'mh', 'bh', 'ah', 'up', 'down', 'left', 'right', 'ppc', 'mg', 'gauss', 'lbx',
                             'uac5', 'ac5', 'ac10', 'ac20')

    bodyDonorFilter = ('eh', 'mh', 'bh', 'ah', 'pCone', 'pSphere', 'polySurface')
    bodyDonorFilterHbsWhy = ('eh', 'mh', 'bh', 'ah', 'up', 'down', 'left', 'right', 'pCone', 'pSphere', 'polySurface',
                             'ppc', 'mg', 'gauss', 'lbx','uac5', 'ac5', 'ac10', 'ac20')

    weaponsFilter = ('eh', 'mh', 'bh', 'ah')
    weaponsHbsWhy = ('eh', 'mh', 'bh', 'ah', 'up', 'down', 'left', 'right', 'ppc', 'mg', 'gauss', 'lbx',
                     'uac5', 'ac5', 'ac10', 'ac20')

    def __init__(self, logger, parent=None):
        """

        :param settings:
        :type settings: RtlSettings
        :param logger:
        :type logger: logging.Logger
        :param parent:
        :type parent:
        """
        super(BkProcessor, self).__init__(parent)
        self.Logger = logger
        self.StartBundleLoad.connect(self._loadBundles)
        self.emipGen = EmipGenerator()
        self.PartsBundle = ''
        self.DonorBundle = ''
        self.BlankFile = ''
        self.BlankData = b''
        self.PartsAsset = None # type: AssetData
        self.DonorAsset = None # type: AssetData
        self.BlankExplodes = False
        self.ApplyBody = True
        self.Applyweapons = False
        self.AltWeaponPattern = False
        self.BaseDmgParts = True
        self.hbsWhyMode = False
        self.blankUnmatchedWeapons = False

    def _mapNameBody(self, searchName, otherName):
            if self.BaseDmgParts:
                if searchName == otherName or searchName == otherName.replace('_dmg', '') or searchName == otherName.replace(
                'centre', 'center') or searchName == otherName.replace('_dmg', '').replace('centre', 'center'):
                    return True
            else:
                if searchName == otherName or searchName == otherName.replace('centre', 'center'):
                    return True
            if not self.BlankExplodes:
                if searchName == otherName or searchName == otherName.replace('_explode', ''):
                    return True
            return False

    def _applyWeapons(self):
        toInsert = self.PartsAsset.getObjectsFiltered(self.weaponsFilter, True)

        toReplace = self.DonorAsset.getObjectsFiltered(self.weaponsFilter, True)

        blankMap = []

        if self.hbsWhyMode:
            toInsert = self.PartsAsset.getObjectsFiltered(self.weaponsHbsWhy, True)

            toReplace = self.DonorAsset.getObjectsFiltered(self.weaponsHbsWhy, True)

        # print('----------')
        # for item in toInsert:
        #     print(f'isert: {item.name}, {item.id}, {item.compName}')
        # print('----------')
        # for item in toReplace:
        #     print(f'rplac: {item.name}, {item.compName}')
        # print('----------')
        remap = {}
        matched = []
        for item in self.DonorAsset.bundleManifest:
            rmap = item.getMap()
            isert = None
            rpac = None
            if rmap[1] != '':  # this item might be mappable:
                for weap in toInsert:
                    if weap.compName == rmap[0]:
                        isert = weap
                        break
                else:
                    self.Logger.debug("Missed mapping to part: {0}".format(rmap[0]))
                for weap in toReplace:
                    if weap.compName == rmap[1]:
                        rpac = weap
                        self.Logger.info('Matched using std compare: {0} -> {1}'.format(weap.name, rmap[1]))
                        break
                    elif weap.altCompare(rmap[1], rmap[0], rmap[2], self.hbsWhyMode):
                        self.Logger.info('Matched using alt compare: {0} -> {1}'.format(weap.name, rmap[1]))
                        rpac = weap
                        break
                else:
                    self.Logger.debug("Missed mapping to donor: {0}".format(rmap[1]))
                if (isert is not None) and (rpac is not None):
                    if isert.name not in remap:
                        remap[isert.name] = [rpac.name]
                        matched.append(rpac.name)
                    else:
                        self.Logger.warning('Encountered Duplicate Re-Map: {0}'.format(item.name))
                else:
                    self.Logger.info('Disgarding donor asset: {0}, {1}'.format(rmap[0], rmap[1]))
        if self.AltWeaponPattern:
            for item in toInsert:
                for other in toReplace:
                    if item.name == other.name:
                        remap[item.name] = [other.name]
                        matched.append(other.name)
                        self.Logger.info('Found Weapon using alt match: {0}'.format(item.name))
        # pprint.pprint(remap)
        if self.blankUnmatchedWeapons:
            for item in toReplace:
                if item.name not in matched:
                    blankMap.append(item.name)
                    self.Logger.info(f'Blanking un-used weapon prefab: {item.name}')
        self.emipGen.addReplacements(remap, toInsert, toReplace)
        self.emipGen.addBlanks(self.BlankData, blankMap, toReplace)

    def _applyBody(self):
        toInsert = self.PartsAsset.getObjectsFiltered(self.bodyPartsFilter, False)

        toReplace = self.DonorAsset.getObjectsFiltered(self.bodyDonorFilter, False)

        if self.hbsWhyMode:
            toInsert = self.PartsAsset.getObjectsFiltered(self.bodyPartsFilterHbsWhy, False)

            toReplace = self.DonorAsset.getObjectsFiltered(self.bodyDonorFilterHbsWhy, False)

        replaceMap = {}
        blankMap = []
        remapped = []

        for item in toInsert:
            try:
                searchName = item.name.split('_', 1)[1]
            except Exception:
                self.Logger.critical("SearchName Exception:")
                self.Logger.critical("Item Name: {0}".format(item.name))
                for line in traceback.format_exc().split("\n"):
                    self.Logger.critical(line)
                continue
            for otherItem in toReplace:
                otherName = otherItem.name.split('_', 1)[1]
                if self._mapNameBody(searchName, otherName):
                    if item.name in replaceMap:
                        replaceMap[item.name].append(otherItem.name)
                    else:
                        replaceMap[item.name] = [otherItem.name]
                    remapped.append(otherItem.name)

        for item in toReplace:
            if item.name not in remapped:
                blankMap.append(item.name)

        self.emipGen.addReplacements(replaceMap, toInsert, toReplace)
        self.emipGen.addBlanks(self.BlankData, blankMap, toReplace)

    def _loadBundles(self):
        self.Logger.info('---- Bundle Loading ----')
        try:
            with open(self.BlankFile, 'rb') as blankFile:
                self.BlankData = blankFile.read()

            self.BkUpdate.emit("Loading Parts Bundle...")
            self.PartsAsset = AssetData('Mesh', self.PartsBundle)
            try:
                self.PartsAsset.load()
            except:
                self.Logger.critical("Unhandled Parts Bundle Exception:")
                for line in traceback.format_exc().split("\n"):
                    self.Logger.critical(line)
                self.BundleLoadDone.emit(self.PartsFailed)

            self.BkUpdate.emit("Loading Donor Bundle...")
            self.DonorAsset = AssetData('Mesh', self.DonorBundle)
            try:
                self.DonorAsset.load()
            except:
                self.Logger.critical("Unhandled Donor Bundle Exception:")
                for line in traceback.format_exc().split("\n"):
                    self.Logger.critical(line)
                self.BundleLoadDone.emit(self.DonorFailed)

            self.BkUpdate.emit("Calculating Remap")
            self.emipGen = EmipGenerator(assetName=self.DonorAsset.name)

            if self.ApplyBody:
                self._applyBody()
            if self.Applyweapons:
                self._applyWeapons()

            self.BundleLoadDone.emit(self.Success)
        except:
            self.Logger.critical("Unhandled Bundle Load Exception:")
            for line in traceback.format_exc().split("\n"):
                self.Logger.critical(line)
            self.BundleLoadDone.emit(self.Failed)




